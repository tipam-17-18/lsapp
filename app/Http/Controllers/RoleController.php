<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class RoleController extends Controller
{
    public function index(Request $request)
    {
          $user = $request->user();
          
            //    return view('home');
           //  return redirect('students');
        if($user && $user->role == 'teacher'){
            return view ('enseignant');
         }   
        elseif($user && $user->role == 'etudiant')
        return view ('etudiant');
        else return redirect('/dashboard');     
    }
}
